﻿using System;

namespace P1
{
    public class room
    {
       
        private string type;
        private double length;
        private double width;
        private double height;

        public room()
        {
            type = "Default";
            length = 10;
            width = 10;
            height = 10;
            Console.WriteLine("Creating default room with default constructor: ");
        }

        public room(string typeIn, double lengthIn, double widthIn, double heightIn)
        {
            type = typeIn;
            length = lengthIn;
            width = widthIn;
            height = heightIn;
            Console.WriteLine("Creating room with parameterized constructor: ");
        }

        public void Display ()
        {
            Console.WriteLine("Room Type:" + this.GetType());
            Console.WriteLine("Room Length:" + this.GetLength());
            Console.WriteLine("Room Width: " + this.GetWidth());
            Console.WriteLine("Room Height:" + this.GetHeight());
            Console.WriteLine("Room Area:" + this.GetArea());
            Console.WriteLine("Room Volume:" + this.GetVolume(true).ToString("F2") + " sq ft");
            Console.WriteLine("Room Volume:" + this.GetVolume(false).ToString("F2") + " sq ft");
        }

        public double Convert (string input)
        {
            string dinput;
            double dval = 1;
            bool status = double.TryParse(input, out dval);
            while (status == false || dval <= 0)
            {
                Console.Write("Enter a new value: ");
                dinput = Console.ReadLine();
                status = double.TryParse(dinput, out dval);
            }
            return dval;
        }

        public double Convert (double input)
        {
            if (input <= 0)
            {
                Console.WriteLine("Room size cannot be negative or zero.");
                return 10;
            }
            else
            {
                return input;
            }

        }

        public void SetType (string input)
        {
            type = input;
        }

        public bool SetLength(double input)
        {
            length = input;
            return true;
        }

        public void SetWidth(double input)
        {
            width = input;
            return;
        }

        public void SetHeight(double input)
        {
            height = input;
            return;
        }

        public string GetType()
        {
            return type;
        }

        public double GetWidth()
        {
            return width;
        }

        public double GetHeight()
        {
            return height;
        }

        public double GetLength()
        {
            return length;
        }

        public double GetArea()
        {
            double area = length * width;
            return area;
        }

        public double GetVolume(bool unit = true)
        {
            double volume = length*width*height;
            if (unit == true)
            {
                return volume;
            }
            else if (unit == false)
            {
                volume = volume/27;
                return volume;
            }
            return 0;

        }

    }
}