﻿using System;

namespace P1
{
    public class Program
    {
        public static void Main(string[] args)
        {



                Console.WriteLine("///////////////////////////////////////////////////////");
                Console.WriteLine("P1: Room Class");
                Console.WriteLine("Author: Nikhil Bosshardt");
                Console.WriteLine("Now: " + DateTime.Now.DayOfWeek + ", " + DateTime.Now);
                Console.WriteLine("///////////////////////////////////////////////////////");
                Console.WriteLine();
           

                //START FOR REAL
                room room1 = new room();
                Console.WriteLine();

                room1.Display();
                Console.WriteLine();

                Console.WriteLine("Modify Default room data: ");

                Console.Write("Room type : ");
                room1.SetType(Console.ReadLine());

                Console.Write("Room Length : ");
                room1.SetLength(room1.Convert(Console.ReadLine()));

                Console.Write("Room Width : ");
                room1.SetWidth(room1.Convert(Console.ReadLine()));

                Console.Write("Room Height : ");
                room1.SetHeight(room1.Convert(Console.ReadLine()));

                Console.WriteLine();

                Console.WriteLine("Display rooms new member data: ");
                room1.Display();
                Console.WriteLine();

                room room2 = new room("Bath", 13, 13, 13);
                Console.WriteLine();

                room2.Display();
                Console.WriteLine();


                //Quit Control
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();

        }
    }
}