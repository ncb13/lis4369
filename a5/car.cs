﻿using System;

namespace q11
{
    public class car : vehicle
    {

       
       private string style_;

       public string style
       {
        	get {return style_;}
        	set {style_ = value;}
       }

       public car ()
       {
       		style = "Default";
       		Console.WriteLine("\nCreating object from default car constructor!\n");
       }

       public car (string mn = "Manufacturer", string mk = "Make", string md = "Model", string st = "Style"):
       base (mn, mk, md)
       {
       		this.style = st;
       		Console.WriteLine("\nCreating object from param car constructor!\n");
       }

       public override string getObjectInfo()
       {
       		return base.getObjectInfo();
       }

       public override string getObjectInfo (char sep)
       {
       		return base.getObjectInfo(sep) + sep + style;
       }
    }
}