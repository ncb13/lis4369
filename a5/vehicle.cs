﻿using System;

namespace q11
{
    public class vehicle
    {
       
        private float milesTraveled = 1.0f;
        private float gallonsUsed = 1.0f;
        

        public string manufacturer {get; set;}
        public string make {get; set;}
        public string model {get; set;}

        public vehicle ()
        {
        	this.manufacturer = "GM";
        	this.make = "Chevrolet";
        	this.model = "Camero";
        	Console.WriteLine("\nCreating object from default vehicle constructor!\n");
        }

        public vehicle (string mn = "GM", string mk = "Chevrolet", string md = "Camero")
        {
        	manufacturer = mn;
        	make = mk;
        	model = md;
        	Console.WriteLine("\nCreating object from param vehicle constructor!\n");
        }

        public void setMiles(float m = 0.0f)
        {
        	milesTraveled = m;
        }

        public void setGallons(float m = 0.0f)
        {
        	gallonsUsed = m;
        }

        public float getMiles()
        {
        	return milesTraveled;
        }

        public float getGallons()
        {
        	return gallonsUsed;
        }

        public float MPG
        {
        	get{
        		if (gallonsUsed <= 0.0f)
        		{
        			return 1.0f;
        		}
        		else
        		{
        			return milesTraveled/gallonsUsed;
        		}
        	}
        }

        public virtual string getObjectInfo ()
        {
        	return manufacturer + " - " + make + " - " + model + " - " + MPG;
        }

        public virtual string getObjectInfo (char sep)
        {
        	return manufacturer + sep + make + sep + model + sep + MPG;
        }

    }
}