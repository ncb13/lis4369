using System;

namespace q11
{
    public class Program
    {
        public static void Main(string[] args)
        {	
        		//
        		//
        		//
                Console.WriteLine("///////////////////////////////////////////////////////");
                Console.WriteLine("Q11: Vehicle class with Polymorphism");
                Console.WriteLine("Author: Nikhil Bosshardt");
                Console.WriteLine("Now: " + DateTime.Now.DayOfWeek + ", " + DateTime.Now);
                Console.WriteLine("///////////////////////////////////////////////////////");
                Console.WriteLine();
                //
                //
                string manu = "F";
                string make = "F";
                string model = "F";
                string miles = "F";
                string gallons = "F";
                string sep = "F";
                string style = "F";
                char sep_c;
                float miles_f;
                float gallons_f;
                Console.WriteLine();
                Console.Write("Setting up car 1! All Default: ");
                vehicle carBase = new vehicle();
                Console.WriteLine(carBase.getObjectInfo());
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("User input: call param constructor:");
                Console.Write("Manufacturer: ");
                manu = Console.ReadLine();
                Console.Write("Make: ");
                make = Console.ReadLine();
                Console.Write("Model: ");
                model = Console.ReadLine();                
                
                Console.Write("Miles Driven: ");                
                miles = Console.ReadLine();
                while (float.TryParse(miles, out miles_f) == false)
                    {
                        Console.Write("Enter a float value: ");
                        miles = Console.ReadLine();
                    }

                Console.Write("Gallons used: ");
                gallons = Console.ReadLine();
                
                while (float.TryParse(gallons, out gallons_f) == false)
                    {
                        Console.Write("Enter a float value: ");
                        gallons = Console.ReadLine();
                    }
                Console.WriteLine();
                Console.WriteLine();
                Console.Write("Setting up car 2! This one only has the manufactuerer: ");
                vehicle car1 = new vehicle(manu);


                Console.Write("Enter a seporator for the car1 GetObjectInfo: ");
                sep = Console.ReadLine();
                while (char.TryParse(sep, out sep_c) == false)
                    {
                        Console.Write("Enter a char value: ");
                        sep = Console.ReadLine();
                    }
                Console.WriteLine(car1.getObjectInfo(sep_c));
                Console.WriteLine();
                Console.WriteLine();
                Console.Write("Setting up car 3! This one has everything BUT style: ");
                vehicle car2 = new vehicle(manu, make, model);
                car2.setGallons(gallons_f);
                car2.setMiles(miles_f);

                Console.Write("Enter a seporator for the car2 GetObjectInfo: ");
                sep = Console.ReadLine();
                while (char.TryParse(sep, out sep_c) == false)
                    {
                        Console.Write("Enter a char value: ");
                        sep = Console.ReadLine();
                    }
                Console.WriteLine(car2.getObjectInfo(sep_c));

                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("Setting up car 4! This one has style: ");
                Console.Write("Manufacturer: ");
                manu = Console.ReadLine();
                Console.Write("Make: ");
                make = Console.ReadLine();
                Console.Write("Model: ");
                model = Console.ReadLine();
                Console.Write("Style: ");
                style = Console.ReadLine(); 

                Console.Write("Miles Driven: ");                
                miles = Console.ReadLine();
                while (float.TryParse(miles, out miles_f) == false)
                    {
                        Console.Write("Enter a float value: ");
                        miles = Console.ReadLine();
                    }
                car1.setMiles(miles_f);

                Console.Write("Gallons used: ");
                gallons = Console.ReadLine();
                
                while (float.TryParse(gallons, out gallons_f) == false)
                    {
                        Console.Write("Enter a float value: ");
                        gallons = Console.ReadLine();
                    }

                car car3 = new car(manu, make, model, style);
                car3.setGallons(gallons_f);
                car3.setMiles(miles_f);

                Console.Write("Enter a seporator for the car2 GetObjectInfo: ");
                sep = Console.ReadLine();
                while (char.TryParse(sep, out sep_c) == false)
                    {
                        Console.Write("Enter a char value: ");
                        sep = Console.ReadLine();
                    }
                Console.WriteLine(car3.getObjectInfo(sep_c));





				//Quit Control
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
        }
    }
}
