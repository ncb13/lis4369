# LIS 4369 - Extensible Enterprise Solutions

## Nikhil Bosshardt

### Assignments 1-5, Project 1 and 2

[A1 README!](a1/README.md)

* Installing asp.net core
* Running basic asp.net app
* Learning bitbucket commands
* Bitbucket Tutorials 

[A2 README!](a2/README.md)

* Simple calculator app with screenshots

[A3 README!](a3/README.md)

* A future value calculator with functions
* Screenshots also included

[P1 README!](p1/README.md)

* Room size calculator with function
* External class example
* Screenshots included

[A4 README!](a4/README.md)

* Student and person class running with screenshots
* Polymorphism and Inheritence example

[A5 README!](a5/README.md)

* Car and vehicle running with screenshots
* More polymorphism and Inheritence example

[P2 README!](p2/README.md)

* Various Linq Demos