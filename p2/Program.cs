﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace p2
	{
	public class Program
		{

    public static void Main()
   	{
        
        		//
        		//
        		//
                Console.WriteLine("///////////////////////////////////////////////////////");
                Console.WriteLine("P2: Working with Linq");
                Console.WriteLine("Author: Nikhil Bosshardt");
                Console.WriteLine("Now: " + DateTime.Now.DayOfWeek + ", " + DateTime.Now);
                Console.WriteLine("///////////////////////////////////////////////////////");
                Console.WriteLine();
                //
                //


        var people = GenerateListOfPeople();

        Console.WriteLine("***Finding items in Collections***");

        Console.WriteLine("\nWhere:");
        //There will be two Persons in this variable: the "Steve" Person and the "Jane" Person
        var peopleOverTheAgeOf30 = people.Where(x => x.Age > 30);
        foreach(var person in peopleOverTheAgeOf30)
        {
            Console.WriteLine(person.FirstName);
        }

        Console.WriteLine("\nSkip:");
        //Will ignore Eric and Steve in the list of people
        IEnumerable<Person> afterTwo = people.Skip(2);
        foreach(var person in afterTwo)
        {
            Console.WriteLine(person.FirstName);
        }

        Console.WriteLine("\nTake:");
        //Will only return Eric and Steve from the list of people
        IEnumerable<Person> takeTwo = people.Take(2);
        foreach(var person in takeTwo)
        {
            Console.WriteLine(person.FirstName);
        }

        Console.ReadKey();

        Console.WriteLine("\n***Changing each item in Collections***");

        Console.WriteLine("\nSelect:");
        IEnumerable<string> allFirstNames = people.Select(x => x.FirstName);
        foreach(var firstName in allFirstNames)
        {
            Console.WriteLine(firstName);
        }

        Console.WriteLine("\nFullName class and Objects:");
        IEnumerable<FullName> allFullNames = people.Select(x => new FullName { First = x.FirstName, Last = x.LastName });
        foreach(var fullName in allFullNames)
        {
            Console.WriteLine($"{fullName.Last}, {fullName.First}");
        }

        Console.ReadKey();

        Console.WriteLine("\n***Changing each item in Collections***");

        Console.WriteLine("\nFirst or default:");
        Person firstOrDefault = people.FirstOrDefault();
        Console.WriteLine(firstOrDefault.FirstName);

        Console.WriteLine("\nFirst or default as filter:");
        var firstThirtyYearOld1 = people.FirstOrDefault(x => x.Age == 30);
		var firstThirtyYearOld2 = people.Where(x => x.Age == 30).FirstOrDefault();
		Console.WriteLine(firstThirtyYearOld1.FirstName); //Will output "Brendan"
		Console.WriteLine(firstThirtyYearOld2.FirstName); //Will also output "Brendan"

		Console.WriteLine("\nHow orDefault Works:");
		List<Person> emptyList = new List<Person>();
        Person willBeNull = emptyList.FirstOrDefault();

        
        Person willAlsoBeNull = people.FirstOrDefault(x => x.FirstName == "John"); 

        Console.WriteLine(willBeNull == null); // true
        Console.WriteLine(willAlsoBeNull == null); //true


        Console.WriteLine("\nLast or default as filter:");
        Person lastOrDefault = people.LastOrDefault();
        Console.WriteLine(lastOrDefault.FirstName);
        Person lastThirtyYearOld = people.LastOrDefault(x => x.Age == 30);
        Console.WriteLine(lastThirtyYearOld.FirstName);

        Console.WriteLine("\nSingleOrdefault as filter:");
        Person single = people.SingleOrDefault(x => x.FirstName == "Eric"); 
        Console.WriteLine(single.FirstName);
        // Uncomment the next line to see it throw an exception
        // Person singleDev = people.SingleOrDefault(x => x.Occupation == "Dev");

        Console.ReadKey();

        Console.WriteLine("\n***Finding Data about Collections***");

        Console.WriteLine("\nCount():");
        int numberOfPeopleInList = people.Count();
        Console.WriteLine(numberOfPeopleInList);

        Console.WriteLine("\nCount() with predicate expression:");
        int peopleOverTwentyFive = people.Count(x => x.Age > 25);
        Console.WriteLine(peopleOverTwentyFive);

        Console.WriteLine("\nAny():");
        bool thereArePeople = people.Any();
        Console.WriteLine(thereArePeople);
        bool thereAreNoPeople = emptyList.Any();
        Console.WriteLine(thereAreNoPeople);

        Console.WriteLine("\nAll():");
        bool allDevs = people.All(x => x.Occupation == "Dev");
        Console.WriteLine(allDevs);
        bool everyoneAtLeastTwentyFour = people.All(x => x.Age >= 24);
        Console.WriteLine(everyoneAtLeastTwentyFour);

        Console.ReadKey();

        Console.WriteLine("\n***Converting results to Collections***");

        Console.WriteLine("\nToList():");
        List<Person> listOfDevs = people.Where(x => x.Occupation == "Dev").ToList(); //This will return a List<Person>
        foreach(var person in listOfDevs)
        {
            Console.WriteLine(person.FirstName);
        }



        Console.WriteLine("\nToArray():");
        Person[] arrayOfDevs = people.Where(x => x.Occupation == "Dev").ToArray(); //This will return a Person[] array
        foreach(var person in arrayOfDevs)
        {
            Console.WriteLine(person.FirstName);
        }


				//Quit Control
                Console.WriteLine("\nPress any key to exit...");
                Console.ReadKey();




        
    }

    public static List<Person> GenerateListOfPeople()
    {
        var people = new List<Person>();

        people.Add(new Person { FirstName = "Eric", LastName = "Fleming", Occupation = "Dev", Age = 24 });
        people.Add(new Person { FirstName = "Steve", LastName = "Smith", Occupation = "Manager", Age = 40 });
        people.Add(new Person { FirstName = "Brendan", LastName = "Enrick", Occupation = "Dev", Age = 30 });
        people.Add(new Person { FirstName = "Jane", LastName = "Doe", Occupation = "Dev", Age = 35 });
        people.Add(new Person { FirstName = "Samantha", LastName = "Jones", Occupation = "Dev", Age = 24 });

        return people;
    }
}

public class Person
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Occupation { get; set; }
    public int Age { get; set; }
}

public class FullName
{
    public string First { get; set; }
    public string Last { get; set; }
}
}
