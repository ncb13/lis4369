using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
               



                Console.WriteLine("///////////////////////////////////////////////////////");
                Console.WriteLine("A2: Simple Calculator");
                Console.WriteLine("Note: Program does not preform data validation.");
                Console.WriteLine("Author: Nikhil Bosshardt");
                Console.WriteLine("Now: " + DateTime.Now.DayOfWeek + ", " + DateTime.Now);
                Console.WriteLine("///////////////////////////////////////////////////////");
                Console.WriteLine();

                Console.Write("Num1: ");
                double num1;
                Double.TryParse(Console.ReadLine(), out num1);
                
                Console.Write("Num2: ");
                double num2;
                Double.TryParse(Console.ReadLine(), out num2);

                Console.WriteLine("1 - Addition");
                Console.WriteLine("2 - Subtraction");
                Console.WriteLine("3 - Multiplication");
                Console.WriteLine("4 - Division");

                Console.Write("Choose a mathematical operation: ");

                int choice;
                Int32.TryParse(Console.ReadLine(), out choice);

                while (num2 == 0 && choice == 4)
                {
                    Console.Write("Divide by zero error! Retype number 2: ");
                    Double.TryParse(Console.ReadLine(), out num2);
                }

                if      (choice == 1)
                {
                	Console.WriteLine("***Result of Addition Operation***");
                	Console.WriteLine(num1+num2);
                }
                else if (choice == 2)
                {
                	Console.WriteLine("***Result of Subtration Operation***");
                	Console.WriteLine(num1-num2);
                }
                else if (choice == 3)
                {
                	Console.WriteLine("***Result of Multiplication Operation***");
                	Console.WriteLine(num1*num2);
                }
                else if (choice == 4)
                {
                	Console.WriteLine("***Result of Division Operation***");
                	Console.WriteLine(num1/num2);
                }
                else
                {
                	Console.WriteLine("An incorrect mathematical operation was entered!");
                }
               
                //Quit Control
                Console.WriteLine("Press any key to exit...");
				Console.ReadKey();
        }
    }
}
