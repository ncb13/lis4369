﻿using System;

namespace a4
{
    public class student : person
    {
       
        private string college;
        private string major;
        private double gpa;

        public student()
        {
            Console.WriteLine("*Base student created*");
            college = "FSU";
            major = "Undecided";
            gpa = 4.00;
            this.SetFname("First Name");
            this.SetLname("Last Name");
            this.SetAge(21);
        }

        public student(string f, string l, int a, string c, string m, double g)
        {
            Console.WriteLine("*Param person created*");
            gpa = g;
            college = c;
            major = m;
            this.SetAge(a);
            this.SetLname(l);
            this.SetFname(f);
        }

        public override string GetObjectInfo()
        {
            return this.GetFname() + " " + this.GetLname() + " is " +
            this.GetAge() + " in the college of " + college + ", majoring in "  +
            major + ", and has a " + string.Format("{0:0.00}", gpa) + " gpa.";
        }

        public new double Convert(string input)
        {
            string dinput;
            double dval = 1;
            bool status = double.TryParse(input, out dval);
            while (status == false || dval <= 0)
            {
                Console.Write("Enter a new value: ");
                dinput = Console.ReadLine();
                status = double.TryParse(dinput, out dval);
            }
            return dval;
        }

        public string GetCollege()
        {
            return college;
        }
        
        public string GetMajor()
        {
            return major;
        }

        public double GetGPA()
        {
            return gpa;
        }

        public void SetCollege(string input)
        {
            college = input;
        }

        public void SetMajor(string input)
        {
            major = input;
        }

        public void SetGPA(double input)
        {
            gpa = input;
        }

    }
}