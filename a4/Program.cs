using System;

namespace a4
{
    public class Program
    {
        public static void Main(string[] args)
        {	
        		//
        		//
        		//
                Console.WriteLine("///////////////////////////////////////////////////////");
                Console.WriteLine("A4: Student class with Polymorphism");
                Console.WriteLine("Author: Nikhil Bosshardt");
                Console.WriteLine("Now: " + DateTime.Now.DayOfWeek + ", " + DateTime.Now);
                Console.WriteLine("///////////////////////////////////////////////////////");
                Console.WriteLine();
                //
                //
                Console.WriteLine("Creating first name last name person object from default constructor:");
                person person1 = new person();
                Console.WriteLine("First name: " + person1.GetFname());
                Console.WriteLine("First name: " + person1.GetLname());
                Console.WriteLine("Age: " + person1.GetAge());
                Console.WriteLine();
                //
                Console.WriteLine("Modify person objects data created with defualt constructor: ");
                Console.Write("First name: ");
                person1.SetFname(Console.ReadLine());
                Console.Write("Last name: ");
                person1.SetLname(Console.ReadLine());
                Console.Write("Age: ");
                person1.SetAge(person1.Convert(Console.ReadLine()));
                Console.WriteLine();
                //
                Console.WriteLine("Call new data members: ");
                Console.WriteLine("First name: " + person1.GetFname());
                Console.WriteLine("First name: " + person1.GetLname());
                Console.WriteLine("Age: " + person1.GetAge());
                Console.WriteLine();
                //
                Console.WriteLine("Parameterized constructor (accepts arguments): ");
                string fname1 = " ";
                string lname1 = " ";
                int age1 = 0;
                Console.Write("First name: ");
                fname1 = Console.ReadLine();
                Console.Write("Last name: ");
                lname1 = Console.ReadLine();
                Console.Write("Age: ");
                age1 = person1.Convert(Console.ReadLine());
                Console.WriteLine();
                person person2 = new person(fname1, lname1, age1);
				//
                Console.WriteLine("Call new data members from Parameterized constructor: ");
                Console.WriteLine("First name: " + person2.GetFname());
                Console.WriteLine("First name: " + person2.GetLname());
                Console.WriteLine("Age: " + person2.GetAge());
                Console.WriteLine();
                //
                Console.WriteLine("Calling student constructor to demonstrate inheritence: ");
                student student1 = new student();
                Console.WriteLine("First name: " + student1.GetFname());
                Console.WriteLine("First name: " + student1.GetLname());
                Console.WriteLine("Age: " + student1.GetAge());
                Console.WriteLine();
                //
                Console.WriteLine("Demonstrating Polymorphism: ");
                
                string fname2 = " ";
                string lname2 = " ";
                int age2 = 0;
                string college2 = " ";
                string major2 = " ";
                double gpa2 = 0.0;

                Console.Write("First name: ");
                fname2 = Console.ReadLine();
                Console.Write("Last name: ");
                lname2 = Console.ReadLine();
                Console.Write("Age: ");
                age2 = person1.Convert(Console.ReadLine());
                Console.Write("College: ");
                college2 = Console.ReadLine();
                Console.Write("Major: ");
                major2 = Console.ReadLine();
                Console.Write("GPA: ");
                gpa2 = student1.Convert(Console.ReadLine());
                //
                Console.WriteLine();
				student student2 = new student(fname2, lname2, age2, college2, major2, gpa2);
				Console.WriteLine();
				//
				Console.Write("Person2 - GetObjectInfo (virtual): ");
				Console.WriteLine(person2.GetObjectInfo());
				Console.WriteLine();
				//
				Console.Write("Student2 - GetObjectInfo (overridden): ");
				Console.WriteLine(student2.GetObjectInfo());
				Console.WriteLine();



				//Quit Control
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
        }
    }
}
