﻿using System;

namespace a4
{
    public class person
    {
       
        private string fname;
        private string lname;
        private int age;

        
        public person ()
        {
            Console.WriteLine("*Base person created*");
            fname = "First Name";
            lname = "Last Name";
            age = 21;
        }

        public person (string f, string l, int a)
        {
            Console.WriteLine("*Param person created*");
            fname = f;
            lname = l;
            age = a;
        }

        public int Convert (string input)
        {
            string dinput;
            int ival = 100;
            bool status = Int32.TryParse(input, out ival);
            while (status == false || ival <= 0)
            {
                Console.Write("Enter a new value: ");
                dinput = Console.ReadLine();
                status = int.TryParse(dinput, out ival);
            }
            return ival;
        }

        public virtual string GetObjectInfo ()
        {
            return this.fname + " " + this.lname + " is " + this.age + ".";
        }
        public void SetFname (string input)
        {
            fname = input;
        }

        public void SetLname (string input)
        {
            lname = input;
        }

        public void SetAge (int input)
        {
            age = input;
        }

        public string GetFname ()
        {
            return fname;
        }

        public string GetLname ()
        {
            return lname;
        }

        public int GetAge ()
        {
            return age;
        }
    }
}