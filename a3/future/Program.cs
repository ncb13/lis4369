﻿using System;

namespace ConsoleApplication
{
    public class Program
    {


        public decimal valCalc(decimal bal, int years, decimal rate, decimal deposit)
        {
                double dbal = (double)bal;
                double ddeposit = (double)deposit;
                double drate = (double)rate;
                double fv;
                double t = 12*years;
                double rate1 = drate + 1;

                fv = dbal*Math.Pow(rate1,t)+ddeposit*((Math.Pow(rate1,t)-1)/drate)*rate1;
                

            return System.Convert.ToDecimal(fv);
        }


        public static void Main(string[] args)
        {



                Console.WriteLine("///////////////////////////////////////////////////////");
                Console.WriteLine("A3: Future value calculator");
                Console.WriteLine("Author: Nikhil Bosshardt");
                Console.WriteLine("Now: " + DateTime.Now.DayOfWeek + ", " + DateTime.Now);
                Console.WriteLine("///////////////////////////////////////////////////////");
                Console.WriteLine();

               //Get Bal 
               decimal bal;
               bool check = true;
               do
                {
                Console.Write("Starting Balance: ");
                check = Decimal.TryParse(Console.ReadLine(), out bal);
                	if (check == false)
                	{
                		Console.WriteLine("Enter a numeric value!");
                	}
                }
                while(check == false);
               
                //Get term
                int years;
                do
                {
                Console.Write("Term (years): ");
                check = Int32.TryParse(Console.ReadLine(), out years);
                	if (check == false)
                	{
                		Console.WriteLine("Enter a whole number value!");
                	}
                }
                while(check == false);


                //Get rate
                decimal rate;
                do
                {
                Console.Write("Interest Rate: ");
                check = Decimal.TryParse(Console.ReadLine(), out rate);
                	if (check == false || rate > 1)
                	{
                		Console.WriteLine("Enter a numeric value!");
                	}
                }
                while(check == false || rate > 1);

                //Get deposit
                decimal deposit;
                do
                {
                Console.Write("Monthly deposit: ");
                check = Decimal.TryParse(Console.ReadLine(), out deposit);
                	if (check == false)
                	{
                		Console.WriteLine("Enter a numeric value!");
                	}
                }
                while(check == false);

                Program app = new Program();
                Console.WriteLine(string.Format("{0:C}", app.valCalc(bal, years, rate, deposit)));
                			
                
                //Quit Control
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();

        }
    }
}